<?php
   //We are going to render a template, rather than 
   //mix php and html.
   //What this means is that we will 
   // -Load the file
   // -store it as a string
   // -Process the string, by replacing templated values
   // -echo the string
   require_once 'sqlhelper.php';
   require_once './vendor/autoload.php';  //include the twig library.
   $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory

    //Sometimes you have to manually delete the cache
    $twig = new Twig_Environment($loader);

    //Sql setup
    
   $conn = connectToMyDatabase();
   $result = $conn->query("call store_list()");

   //Simple example of routing
   //I can load a different page, if there is an error
   if($result){
      $table = $result->fetch_all(MYSQLI_ASSOC); 
      //setup twig
      $template = $twig->load('frontpage.twig.html');
 
      //call render to replace values in template with ones specified in my array
      //Since the return value is a string, I can echo it.
      echo $template->render(array("stores"=>$table));

      $conn->close(); //clean up connection
                   //Probably autoclose on exit, but just to be safe.
                   //I really wish I had RAII here.
   }else {
    
    //One benefit is that we can load a full error page
    $template = $twig->load("error.twig.html");
    echo $template->render(array("message"=>"SQL errorm query failed"));
   }
?>
<!-- no more html here -->
<!-- leads to cleaner code -->
