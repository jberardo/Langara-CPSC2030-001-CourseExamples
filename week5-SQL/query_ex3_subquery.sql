

/* for this example
   we want find  the items a store is not selling an item
   that is in the warehosue.
   So we want to generate all possible store and item combinations

   Covers two topics. Cross join and exists
   - the cross join, otherwise known full outer join
   - the cross join is the mariadb/mysql version
   - we use it to generate all the possible combinations of stores and items
   - in this case we didn't specify an on condition, so all permutation

   -next we check if the entry is no in the inventory table, 
   -with not exists in the where clause
   */
select s.store,  w.item 
from warehouse as w
cross join stores as s
where not exists( select item from storeinventory as si where si.store = s.store and w.item = si.item)