<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <title>Document</title>
</head>
<?php
//Setup
include "sqlhelper.php";

//database stuff first
   //SQL  portion
   $user = 'store';
   $pwd = 'store';
   $server = 'localhost';
   $dbname = 'store_example';

   $conn = new mysqli($server, $user, $pwd, $dbname);
   $store = mysqli_real_escape_string($conn, $_GET["store"]); //prevent SQL injection

   $result = $conn->query("call merch_table(\"$store\")");  //Need to escape quotes inside quotes
   clearConnection($conn);
   //then generate the table
?>
<body>
<h1>Welcome to <?php echo $store; ?></h1>
<p>For this week we will mix HTML and PHP.  We shouldn't do this and next week we show you how not 
to do this. Remember that we are using PHP to generate HTML code, or parts of HTML code. Notice that things 
get messy really quickly when we do this, and this will leads to errors.  It is also really hard to debug with
multiple php blocks, but you will find such examples on the web. The internet is not to be trusted when
it comes to good PHP code.  Best to keep the PHP as short as possible.

</p>

<?php
    echo "Merch for $store"; //Notice we can substitute variables into strings
                             //only works with variables and not function calls
    if($result){
       //Print the items for sale.
       $table = $result->fetch_all(MYSQLI_ASSOC); //returns a table or rows. Rows are associative arrays
  
       echo "<table>"; //Inside ?php, need to echo html code   
       foreach( $table as $row){
          echo wrap("tr", wrap("td",$row["item"]).wrap("td",$row["price"]).wrap("td",$row["description"]));
       }
       echo "</table>";
    } else {
       echo "error";
    }
    ?>
</body>
</html>